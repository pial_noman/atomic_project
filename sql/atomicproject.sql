-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2016 at 08:10 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdate`
--

CREATE TABLE IF NOT EXISTS `birthdate` (
`ID` int(5) NOT NULL,
  `Name` varchar(10) NOT NULL,
  `Date` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthdate`
--

INSERT INTO `birthdate` (`ID`, `Name`, `Date`) VALUES
(34, 'pial', '19/06/09'),
(35, 'Aunkur', '31/03/2000');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`ID` int(5) NOT NULL,
  `Book` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`ID`, `Book`) VALUES
(2, 'Opekkha by Humayun Ahmed'),
(3, 'Gitanjoli by Rabindranath Tagore'),
(5, 'chelebela'),
(7, 'Java'),
(8, 'Dowr by Shomoresh Mojubdar'),
(9, 'ekhon tokhon manik roton by Jafar Iqbal'),
(10, 'rakkhos khokkos vokkos by Humayun Ahmed');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`ID` int(5) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `City` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`ID`, `Name`, `City`) VALUES
(1, 'pial', 'Rajshahi'),
(3, 'Papel', 'Dhaka'),
(4, 'Urmee', 'Rajshahi');

-- --------------------------------------------------------

--
-- Table structure for table `emailsubscription`
--

CREATE TABLE IF NOT EXISTS `emailsubscription` (
`ID` int(5) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emailsubscription`
--

INSERT INTO `emailsubscription` (`ID`, `Name`, `Email`) VALUES
(9, 'Papel', 'k@boom.com'),
(11, 'pial', 'awronno.adhar@gmail.com'),
(12, 'mithila', 'mithila.shapnil@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`ID` int(5) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Sex` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`ID`, `Name`, `Sex`) VALUES
(1, 'pial', 'Male'),
(6, 'ankur', 'Male'),
(7, 'mithila', 'Female'),
(9, 'asha', 'Female'),
(10, 'Papel', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
`ID` int(7) NOT NULL,
  `Name` varchar(250) NOT NULL,
  `Hobby` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`ID`, `Name`, `Hobby`) VALUES
(6, 'pial', 'Travell'),
(10, 'mithila', 'Reading'),
(12, 'apel', 'Board or Computer Game'),
(13, 'ankur', 'Travelling,Gardening'),
(16, 'ankur', 'Outdoor Game'),
(18, 'pial', 'Travelling,Outdoor Game,Board or Computer Game'),
(19, 'noman', 'Outdoor Game,Reading'),
(20, 'shahriar', 'Outdoor Game,Board or Computer Game'),
(24, 'asha', 'Travelling');

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE IF NOT EXISTS `profilepicture` (
`ID` int(10) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Picture` blob NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`ID`, `Name`, `Picture`) VALUES
(1, '', ''),
(2, 'noman', ''),
(3, 'noman', ''),
(4, 'noman', ''),
(5, 'mithila', '');

-- --------------------------------------------------------

--
-- Table structure for table `summaryoforganization`
--

CREATE TABLE IF NOT EXISTS `summaryoforganization` (
`ID` int(5) NOT NULL,
  `ORG` varchar(30) NOT NULL,
  `Summary` varchar(1024) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summaryoforganization`
--

INSERT INTO `summaryoforganization` (`ID`, `ORG`, `Summary`) VALUES
(3, 'W3c', '                                            W3C is delighted to announce that it will receive a 2016 Technology & Engineering Emmy Â® Award from the National Academy of Television Arts & Sciences for its work on the Timed Text Mark-up Language standard that makes video content more accessible with text captioning and subtitles. Representatives from W3C staff and the Timed Text Working Group will attend the awards ceremony on 8 January at the Bellagio Hotel in Las Vegas during the Consumer Electronics Show (CES).\r\n\r\nâ€œW3C is thrilled to receive a 2016 Emmy Â® Award in recognition of technologies that support an important part of our mission to bring the full potential of the World Wide Web to everyone, whatever their disability, culture, language, device or network infrastructure,â€ said W3C CEO Dr. Jeff Jaffe. â€œI would like to thank the National Academy of Television Arts & Sciences for their recognition of W3C, and I congratulate the members of the W3C Timed Texxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'),
(6, 'WHO', 'The World Health Organization (WHO; /huË/) is a specialized agency of the United Nations (UN) that is concerned with international public health. It was established on 7 April 1948, headquartered in Geneva, Switzerland. The WHO is a member of the United Nations Development Group. Its predecessor, the Health Organization, was an agency of the League of Nations.'),
(7, 'brac', 'Archaeological findings date the existence of human communities on the island back to the palaeolithic (in the KopaÄina cave between Supetar and Donji Humac). Nevertheless, there are no traces of human habitation from the neolithic. In the Bronze Age and Iron Age, Illyrian tribes populated the inner parts of the island. Numerous villages existed at that time (but none of them survived).\r\n\r\nIn the 4th century BC Greek colonisation spread over many Adriatic islands and along the shore, but none of them on BraÄ. Nevertheless, Greeks visited the island and also traded with the Illyric tribes; Greek artifacts were found in the bay of ViÄja near LoÅ¾iÅ¡Ä‡a on the estate of the Rakela-Bugre brothers. Many of the objects belonging to this still unexamined site are now on display in the Archeological Museum of Split. BraÄ lay on the crossroads of several trade routes from Salona (today Solin) to Issa (today Vis) and the Po River.');

-- --------------------------------------------------------

--
-- Table structure for table `termsandcondition`
--

CREATE TABLE IF NOT EXISTS `termsandcondition` (
`ID` int(5) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Terms` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `termsandcondition`
--

INSERT INTO `termsandcondition` (`ID`, `Name`, `Terms`) VALUES
(1, 'asha', 'agreed'),
(4, 'Md. Abdullah Al Noman', 'agreed'),
(5, 'noman', 'agreed'),
(6, 'ankur', 'agreed'),
(7, 'mithila', 'agreed'),
(8, 'mahin', 'agreed'),
(9, 'Urmee', 'agreed');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdate`
--
ALTER TABLE `birthdate`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `emailsubscription`
--
ALTER TABLE `emailsubscription`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `summaryoforganization`
--
ALTER TABLE `summaryoforganization`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `termsandcondition`
--
ALTER TABLE `termsandcondition`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdate`
--
ALTER TABLE `birthdate`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `emailsubscription`
--
ALTER TABLE `emailsubscription`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
MODIFY `ID` int(7) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `summaryoforganization`
--
ALTER TABLE `summaryoforganization`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `termsandcondition`
--
ALTER TABLE `termsandcondition`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
