-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2016 at 07:18 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdate`
--

CREATE TABLE IF NOT EXISTS `birthdate` (
`ID` int(5) NOT NULL,
  `date` varchar(10) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthdate`
--

INSERT INTO `birthdate` (`ID`, `date`, `name`) VALUES
(1, '22/12/15', ''),
(2, '22/12/15', ''),
(3, '20/55/22', ''),
(4, '20/55/22', ''),
(5, '13/09/90', ''),
(6, '25/09/90', ''),
(7, '25/09/90', ''),
(8, '16/03/92', ''),
(9, '19/05/12', ''),
(10, '30/03/2000', ''),
(11, '', ''),
(12, '', ''),
(13, '19/05/12', ''),
(14, '14/02/1558', ''),
(15, '13/09/90', ''),
(16, '19/05/12', ''),
(17, '13/09/90', ''),
(18, '30/12/90', ''),
(19, '17/01/1578', 'pial'),
(20, '16/03/92', 'myth'),
(21, 'pial', '20/55/22'),
(22, '13/09/90', 'dfs'),
(23, '', ''),
(24, '13/09/90', '.emaildfhfgh'),
(25, '13/09/90', '.pial'),
(26, '19/05/12', '.abd'),
(27, '16/03/92', '.mithila'),
(28, '18/05/88', 'ankur'),
(29, '13/09/90', ''),
(30, '16/03/92', 'toy ha elevator'),
(31, '06/02/1998', 'saif');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`ID` int(5) NOT NULL,
  `book` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`ID`, `book`) VALUES
(1, 'himu'),
(2, 'opekkha'),
(3, 'Gitanjoli'),
(4, 'H.S.C bangla boi');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`ID` int(5) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `City` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`ID`, `Name`, `City`) VALUES
(1, 'pial', 'dhaka'),
(2, 'ankur', 'Rangpur');

-- --------------------------------------------------------

--
-- Table structure for table `emailsubscription`
--

CREATE TABLE IF NOT EXISTS `emailsubscription` (
`ID` int(5) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emailsubscription`
--

INSERT INTO `emailsubscription` (`ID`, `Name`, `Email`) VALUES
(9, 'papel', 'k@b.com'),
(11, 'pial', 'awronno.adhar@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`ID` int(5) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Sex` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`ID`, `Name`, `Sex`) VALUES
(1, 'pial', 'Male'),
(2, '', ''),
(3, '', ''),
(4, '', ''),
(5, 'ankur', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
`ID` int(7) NOT NULL,
  `Name` varchar(250) NOT NULL,
  `Hobby1` varchar(100) NOT NULL,
  `Hobby2` varchar(100) NOT NULL,
  `Hobby3` varchar(100) NOT NULL,
  `Hobby4` varchar(100) NOT NULL,
  `Hobby5` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE IF NOT EXISTS `profilepicture` (
`ID` int(10) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Picture` blob NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`ID`, `Name`, `Picture`) VALUES
(1, '', ''),
(2, 'noman', ''),
(3, 'noman', ''),
(4, 'noman', '');

-- --------------------------------------------------------

--
-- Table structure for table `summaryoforganization`
--

CREATE TABLE IF NOT EXISTS `summaryoforganization` (
`ID` int(5) NOT NULL,
  `ORG_name` varchar(30) NOT NULL,
  `ORG_summary` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summaryoforganization`
--

INSERT INTO `summaryoforganization` (`ID`, `ORG_name`, `ORG_summary`) VALUES
(1, 'basis', 'Laudato Si is Pope Francis’ Encyclical on the environment or more formally – On Care for Our Common Home. Laudato Si means “Praise be to you” which is the first line of a canticle by St. Francis that praises God with all of his creation. '),
(2, '', ''),
(3, '', ''),
(4, '', ''),
(5, '', ''),
(6, 'Array', 'Array'),
(7, 'BIST', '\r\ndcxgbzfdcgbhDFGdzh'),
(8, 'BIST', ' gfhxxjnmmmmmmmmmmmmmmpial'),
(9, 'BIST', ' gfhxxjnmmmmmmmmmmmmmmpial'),
(10, 'BIST', ' gfhxxjnmmmmmmmmmmmmmmpial'),
(11, 'BIST', ' gfhxxjnmmmmmmmmmmmmmmpial'),
(12, 'BIST', ' gfhxxjnmmmmmmmmmmmmmmpial'),
(13, 'BIST', '                         pial'),
(14, 'brac', 'Nothing'),
(15, 'brac', 'Nothing 2.');

-- --------------------------------------------------------

--
-- Table structure for table `termsandcondition`
--

CREATE TABLE IF NOT EXISTS `termsandcondition` (
`ID` int(5) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Terms` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `termsandcondition`
--

INSERT INTO `termsandcondition` (`ID`, `Name`, `Terms`) VALUES
(1, 'pial', 'agreed'),
(2, '', ''),
(3, '', ''),
(4, 'pial', 'agreed'),
(5, 'noman', 'agreed');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdate`
--
ALTER TABLE `birthdate`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `emailsubscription`
--
ALTER TABLE `emailsubscription`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `summaryoforganization`
--
ALTER TABLE `summaryoforganization`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `termsandcondition`
--
ALTER TABLE `termsandcondition`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdate`
--
ALTER TABLE `birthdate`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `emailsubscription`
--
ALTER TABLE `emailsubscription`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
MODIFY `ID` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `summaryoforganization`
--
ALTER TABLE `summaryoforganization`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `termsandcondition`
--
ALTER TABLE `termsandcondition`
MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
