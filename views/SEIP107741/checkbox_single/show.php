<?php
        include_once ('../../../vendor/autoload.php');
        
        use app\BITM\SEIP107741\checkbox_single\TermsCondition;
        use app\BITM\SEIP107741\Utility\Utility;

        $Terms1=new TermsCondition();
        $Termss=$Terms1->show($_GET['ID']);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <title>Show</title>
        
        <style>
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
        </style>

    </head>
    <body>
        <div class="container" align="left">
            <h1>Terms details</h1><a id="utility" href="index.php" class="btn btn-primary">T&C List</a><br><br>

                <dl>
                    <dt>Id:</dt>
                    <dd><?php echo $Termss['ID']; ?></dd>
    
                    <dt>Name:</dt>
                    <dd><?php echo $Termss['Name']; ?></dd>
    
                    <dt>Terms:</dt>
                    <dd><?php echo $Termss['Terms']; ?></dd>
                </dl>
        </div>
    </body>
</html>
