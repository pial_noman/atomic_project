        <?php
        include_once ('../../../vendor/autoload.php');
        
        use app\BITM\SEIP107741\text_area\OrganizationSummary;
                
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        
        <title>Create</title>
        
        <style>
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
        </style>
        
    </head>
    <body>
        <div class="container" align="left">
            <h1>ORG Summary</h1><a id="utility" href="index.php" class="btn btn-primary">Summary List</a><br><br>

            <form action="store.php" method="post" role="form">
                         Organization Name: <input type="text" class="form-control" name="ORG"><br>
                         Add Summary: <textarea class="form-control" rows="12" required placeholder="write a summary about the ORG........." name="Summary">
                                      </textarea><br>                        
                          <input class="btn btn-success" type="submit" name="submit" value="Save">       
            </form>
        </div>
    </body>
</html>
