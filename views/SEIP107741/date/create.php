        <?php
        include_once ('../../../vendor/autoload.php');
        
        use app\BITM\SEIP107741\date\BIRTHDAY;
                   
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <title>Create</title>
        
        <style>
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
        </style>
                
          <script>
            $(function() {
            $( "#datepicker" ).datepicker();
            });
          </script>
        
    </head>
    <body>
        <div class="container" align="left">
            <h1>Birthday</h1><a id="utility" href="index.php" class="btn btn-primary">Birthday List</a><br><br>

            <form action="store.php" method="post" role="form">
                Add Name: <input required autofocus type="text" class="form-control" name="Name"><br>
                Add Birth Date: <input required id="datepicker" type="text" class="form-control" name="Date"><br>
                            <input class="btn btn-success" type="submit" name="submit" value="Save">       
            </form>
        </div>
    </body>
</html>
