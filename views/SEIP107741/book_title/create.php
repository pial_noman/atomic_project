<?php
//ini_set('display_errors', 0);
?>
<?php
    include_once ('../../../vendor/autoload.php');
    use app\BITM\SEIP107741\book_title\BookTitle;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        
        <title>Create</title>
        
        <style>
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
        </style>
        
    </head>
    <body>
        <div class="container" align="left">
            <h1>Book title</h1><a id="utility" href="index.php" class="btn btn-primary">Book List</a><br><br>

            <form action="store.php" method="post" role="form">
                Add Book Name: <input class="form-control" type="text" name="Book"><br>              
                               <input class="btn btn-success" type="submit" name="submit" value="Save">
            </form>
        </div>    
    </body>
</html>
