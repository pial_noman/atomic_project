        <?php
        // put your code here
        include_once ('../../../vendor/autoload.php');        
        use app\BITM\SEIP107741\book_title\BookTitle;
                
        $book1=new BookTitle();
        $books=$book1->show($_GET['ID']);         
        ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <title>Edit</title>
        
        <style>
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }            
        </style>
        
    </head>
    <body>
        <div class="container" align="left">
            <form action="update.php" method="post" role="form">
            <h1>Edit Book</h1><a id="utility" href="index.php" class="btn btn-primary">Book List</a><br><br>
                <input type="hidden" name="ID" value="<?php echo $books['ID'];?>"/>
                
                <div>
                   Enter Book Name:<input autofocus="autofocus" 
                                            type="text" 
                                            name="Book"
                                            class="form-control"
                                            required="required"
                                            value="<?php echo $books['Book'];?>"
                                    />
                 </div><br>
                 
                <button class="btn btn-success" type="submit">Save</button>
                <button class="btn btn-warning" type="submit">Save & Add Again</button>
                <input class="btn btn-danger" type="reset" value="Reset" />
            </form> 
        </div>
    </body>
</html>
