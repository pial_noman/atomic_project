        <?php
            include_once ('../../../vendor/autoload.php');        
            use app\BITM\SEIP107741\file\ProfilePicture;             
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        
        <title>Create</title>
        
        <style>
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
            .btn-file {
                position: relative;
                overflow: hidden;
            }
            .btn-file input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                outline: none;
                background: white;
                cursor: inherit;
                display: block;
            }
        </style>
        
    </head>
    <body>
        <div class="container" align="left">
            <h1>profile picture</h1><a id="utility" href="index.php" class="btn btn-primary">profile picture List</a><br><br>

                <form action="store.php" method="post" role="form" enctype='multipart/form-data'>
                    Add Name: <input type="text" class="form-control" name="Name"><br>
                    <span class="btn btn-default btn-file">
                    Select a profile picture<input type='file' name='Picture'><br>
                    </span><br><br>
                    <input class="btn btn-success" type='submit' name='upload_btn' value='upload'>
                </form>
        </div>
    </body>
</html>
