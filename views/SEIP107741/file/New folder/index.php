<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <title>Profile Picture</title>
        
        <style>

            table,th,td
            {
                text-align: center;
                border: 1px solid black;
            }
            #utility1{
                float:left;
            }
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
        </style> 
    </head>
    <body>
        <div class="container" align="center">
            
        <?php
            include_once ('../../../vendor/autoload.php');        
            use app\BITM\SEIP107741\file\ProfilePicture;
                
            $picture1=new ProfilePicture();
            $pictures=$picture1->index();
        ?>
        <h1>Profile Picture</h1><br><a id="utility" href="../../../index.php" class="btn btn-primary">Home</a><br><br><br>
            
            <div align="left">
                <button type="button" class="btn btn-default">
                    <span id="utility1" class="glyphicon glyphicon-search"></span> Search
                </button>
                    <span id="utility"><a href="#" class="btn btn-default">Download as PDF | XL</a> <a href="create.php" class="btn btn-info">Insert Picture</a></span>
            </div><br>
            
            <table class="table">
                
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Picture</th>
                    <th>Action</th>
                </tr>
                
            <?php
               foreach($pictures as $propic){ //$dates catch the DB's data and $bdate is an local variable
            ?>
            
            <tr>
                <td><?php echo $propic['ID']; //$bdate is an local variable?></td>
                <td><?php echo $propic['Name']; //$bdate is an local variable?></td> 
                <td><a href="show.php?ID=<?php echo $propic['ID'];?>"><?php echo $propic['Picture']; //$bdate is an local variable and date is a datafield of DB?></a></td>                
                <td>
                    <a href="show.php?ID=<?php echo $propic['ID'];?>" class="btn btn-info">view</a>
                    <a href="edit.php?ID=<?php echo $propic['ID'];?>" class="btn btn-warning">Edit</a>
                    <a href="delete.php?ID=<?php echo $propic['ID'];?>" class="btn btn-danger">delete</a>
                    <a href="edit.php" class="btn btn-success">Trash/Recover</a>
                    <a href="edit.php" class="btn btn-default">Email to friend</a>
                    <form action="delete.php" method="post">
                        <input type="hidden" name ="ID" value="<?php echo $propic['ID'];?>" class="delete">
                    </form>
                </td>
            </tr>
            
            <?php
               }
            ?>
            
            </table>
        </div>
    </body>
</html>
