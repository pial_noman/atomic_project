-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2016 at 09:09 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `image`
--

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
`id` int(11) NOT NULL,
  `img_name` varchar(500) COLLATE utf16_swedish_ci NOT NULL,
  `img_type` varchar(500) COLLATE utf16_swedish_ci NOT NULL,
  `img_path` blob NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf16 COLLATE=utf16_swedish_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `img_name`, `img_type`, `img_path`) VALUES
(39, 'PHP_Logo.png', 'image/png', 0x66696c65732f5048505f4c6f676f2e706e67),
(47, 'PHP_Logo.png', 'image/png', 0x66696c65732f5048505f4c6f676f2e706e67),
(48, 'images.jpg', 'image/jpeg', 0x66696c65732f696d616765732e6a7067),
(49, 'download.jpg', 'image/jpeg', 0x66696c65732f646f776e6c6f61642e6a7067),
(50, 'download.jpg', 'image/jpeg', 0x66696c65732f646f776e6c6f61642e6a7067),
(53, 'download.jpg', 'image/jpeg', 0x66696c65732f646f776e6c6f61642e6a7067),
(56, '2.jpg', 'image/jpeg', 0x66696c65732f322e6a7067),
(57, '7.jpg', 'image/jpeg', 0x66696c65732f372e6a7067),
(59, 'images.jpg', 'image/jpeg', 0x66696c65732f696d616765732e6a7067),
(60, 'images.jpg', 'image/jpeg', 0x66696c65732f696d616765732e6a7067),
(61, 'PHP_Logo.png', 'image/png', 0x66696c65732f5048505f4c6f676f2e706e67),
(62, 'PHP_Logo.png', 'image/png', 0x66696c65732f5048505f4c6f676f2e706e67),
(63, 'PHP_Logo.png', 'image/png', 0x66696c65732f5048505f4c6f676f2e706e67),
(64, 'PHP_Logo.png', 'image/png', 0x66696c65732f5048505f4c6f676f2e706e67),
(65, 'PHP_Logo.png', 'image/png', 0x66696c65732f5048505f4c6f676f2e706e67);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file`
--
ALTER TABLE `file`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
