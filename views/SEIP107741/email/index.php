<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <title>Email Subscription</title>
        
        <style>

            table,th,td
            {
                text-align: center;
                border: 1px solid black;
            }
            #utility1{
                float:left;
            }
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
        </style> 
    </head>
    <body>
        <div class="container" align="center">
            
        <?php
            include_once ('../../../vendor/autoload.php');        
            use app\BITM\SEIP107741\email\SUBSCRIPTION;
            use app\BITM\SEIP107741\Utility\Utility;
                
            $email1=new SUBSCRIPTION();
            $emails = $email1->index();         
        ?>

        <h1>Email Subscription</h1><br><a id="utility" href="../../../index.php" class="btn btn-primary">Home</a><br><br><br>
            
            <div align="left">
                <button type="button" class="btn btn-default">
                    <span id="utility1" class="glyphicon glyphicon-search"></span> Search
                </button>
                    <span id="utility"><a href="#" class="btn btn-default">Download as PDF | XL</a> <a href="create.php" class="btn btn-info">Add Email</a></span>
            </div><br>
            
            <table class="table">
                
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            
            <?php
               foreach($emails as $email1){ //$dates catch the DB's data and $bdate is an local variable
            ?>
            
            <tr>
                <td><?php echo $email1['ID']; //$bdate is an local variable?></td>
                <td><?php echo $email1['Name']; //$bdate is an local variable?></td> 
                <td><a href="show.php?ID=<?php echo $email1['ID'];?>"><?php echo $email1['Email']; //$bdate is an local variable and date is a datafield of DB?></a></td>                
                <td>
                    <a href="show.php?ID=<?php echo $email1['ID'];?>" class="btn btn-info">view</a>
                    <a href="edit.php?ID=<?php echo $email1['ID'];?>" class="btn btn-warning">Edit</a>
                    <a href="delete.php?ID=<?php echo $email1['ID'];?>" class="btn btn-danger">delete</a>
                    <a href="edit.php" class="btn btn-success">Trash/Recover</a>
                    <a href="edit.php" class="btn btn-default">Email to friend</a>
                    <form action="delete.php" method="post">
                        <input type="hidden" name ="ID" value="<?php echo $email1['ID'];?>" class="delete">
                    </form>
                </td>
            </tr>
            
            <?php
               }
            ?>
            
            </table>
        </div>
                <!--<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>-->
        <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    
    
    $('#message').hide(100);
        </script>
    </body>
</html>
