<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <title>City</title>
        
        <style>

            table,th,td
            {
                text-align: center;
                border: 1px solid black;
            }
            #utility1{
                float:left;
            }
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
        </style> 
    </head>
    <body>
        <div class="container" align="center">
            
        <?php
            include_once ('../../../vendor/autoload.php');        
            use app\BITM\SEIP107741\select\CITY;
                
            $city1 = new CITY(); //$date1 is an object
            $citys = $city1->index(); //$date1 is an object and $dates catch the DB's data         
        ?>

        <h1>City</h1><br><a id="utility" href="../../../index.php" class="btn btn-primary">Home</a><br><br><br>
            
            <div align="left">
                <button type="button" class="btn btn-default">
                    <span id="utility1" class="glyphicon glyphicon-search"></span> Search
                </button>
                    <span id="utility"><a href="#" class="btn btn-default">Download as PDF | XL</a> <a href="create.php" class="btn btn-info">Add City</a></span>
            </div><br>
            
            <table class="table">
                
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Action</th>
                </tr>
            
            <?php
               foreach($citys as $city1){ //$dates catch the DB's data and $bdate is an local variable
            ?>
            
            <tr>
                <td><?php echo $city1['ID']; //$bdate is an local variable?></td>
                <td><?php echo $city1['Name']; //$bdate is an local variable?></td> 
                <td><a href="show.php?ID=<?php echo $city1['ID'];?>"><?php echo $city1['City']; //$bdate is an local variable and date is a datafield of DB?></a></td>                
                <td>
                    <a href="show.php?ID=<?php echo $city1['ID'];?>" class="btn btn-info">view</a>
                    <a href="edit.php?ID=<?php echo $city1['ID'];?>" class="btn btn-warning">Edit</a>
                    <a href="delete.php?ID=<?php echo $city1['ID'];?>" class="btn btn-danger">delete</a>
                    <a href="edit.php" class="btn btn-success">Trash/Recover</a>
                    <a href="edit.php" class="btn btn-default">Email to friend</a>
                    <form action="delete.php" method="post">
                        <input type="hidden" name ="ID" value="<?php echo $city1['ID'];?>" class="delete">
                    </form><br>
                </td>
            </tr>
            
            <?php
               }
            ?>
            
            </table>
        </div>
    </body>
</html>
