        <?php
        include_once ('../../../vendor/autoload.php');
        
        use app\BITM\SEIP107741\select\CITY;
                   
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        
        <title>Create</title>
        
        <style>
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
        </style>
        
    </head>
    <body>
        <div class="container" align="left">
            <h1>City</h1><a id="utility" href="index.php" class="btn btn-primary">City List</a><br><br>

            <form action="store.php" method="post" role="form">
            Add Name: <input required autofocus type="text" class="form-control" name="Name">
            Add City: <select class="form-control" name="City">
                        <option>Dhaka</option>
                        <option>Rajshahi</option>
                        <option>Rangpur</option>
                        <option>Chittagong</option>
                        <option>Khulna</option>
                        <option>Barishal</option>
                        <option>Sylhet</option>
                      </select><br>
                      <input class="btn btn-success" type="submit" name="submit" value="Save">       
            </form>
        </div>
    </body>
</html>
