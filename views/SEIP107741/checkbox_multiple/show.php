<?php
        include_once ('../../../vendor/autoload.php');
        
        use app\BITM\SEIP107741\checkbox_multiple\Hobby;
        use app\BITM\SEIP107741\Utility\Utility;

        $hobby1=new Hobby();
        $hobbys=$hobby1->show($_GET['ID']);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <title>Show</title>
        
        <style>
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
        </style>

    </head>
    <body>
        <div class="container" align="left">
            <h1>Hobby details</h1><a id="utility" href="index.php" class="btn btn-primary">Hobby List</a><br><br>

                <dl>
                    <dt>Id:</dt>
                    <dd><?php echo $hobbys['ID']; ?></dd>
    
                    <dt>Name:</dt>
                    <dd><?php echo $hobbys['Name']; ?></dd>
    
                    <dt>Hobby:</dt>
                    <dd><?php echo $hobbys['Hobby']; ?></dd>
                </dl>
        </div>
    </body>
</html>
