        <?php
        include_once ('../../../vendor/autoload.php');
        
        use app\BITM\SEIP107741\checkbox_multiple\Hobby;
                   
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        
        <title>Create</title>
        
        <style>
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }
        </style>
        
    </head>
    <body>
        <div class="container" align="left">
            <h1>Hobby</h1><a id="utility" href="index.php" class="btn btn-primary">Hobby List</a><br><br>

            <form action="store.php" method="post" role="form">
               Add Name: <input required autofocus type="text" class="form-control" name="Name"><br>
               Add Hobby: <br><input type="checkbox" name="Hobby[]" value="Travelling">Traveling<br>
                                <input type="checkbox" name="Hobby[]" value="Outdoor Game">Outdoor Game<br>
                                <input type="checkbox" name="Hobby[]" value="Board or Computer Game">Board or Computer Game<br>
                                <input type="checkbox" name="Hobby[]" value="Reading">Reading<br>
                                <input type="checkbox" name="Hobby[]" value="Gardening">Gardening<br><br>
                                <input class="btn btn-success" type="submit" name="submit" value="Save">       
            </form>
        </div>
    </body>
</html>