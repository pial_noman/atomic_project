        <?php
        // put your code here
        include_once ('../../../vendor/autoload.php');
        
        use app\BITM\SEIP107741\checkbox_multiple\Hobby;
                
        $hobby1=new Hobby();
        $hobbys=$hobby1->show($_GET['ID']);        

//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <title>Edit</title>
        
        <style>
            #utility{
                float:right;
            }
            html body {
                background-color: #99ffcc;
            }            
        </style>
        
    </head>
    <body>
        <div class="container" align="left">
            <form action="update.php" method="post" role="form">
            <h1>Edit Hobby</h1><a id="utility" href="index.php" class="btn btn-primary">Hobby List</a><br><br>
                    <input type="hidden" name="ID" value="<?php echo $hobbys['ID'];?>"/>
                     
                    <div>
                        Enter Name: <input autofocus="autofocus" 
                                            class="form-control"
                                            type="text" 
                                            name="Name"
                                            required="required"
                                            value="<?php echo $hobbys['Name'];?>"
                                    />
                    </div><br>
                 
                    <div>
                        Add Hobby: <br><input type="checkbox" name="Hobby[]" value="Travelling">Traveling<br>
                                <input type="checkbox" name="Hobby[]" value="Outdoor Game">Outdoor Game<br>
                                <input type="checkbox" name="Hobby[]" value="Board or Computer Game">Board or Computer Game<br>
                                <input type="checkbox" name="Hobby[]" value="Reading">Reading<br>
                                <input type="checkbox" name="Hobby[]" value="Gardening">Gardening
                    </div><br>
                 
                <button class="btn btn-success" type="submit">Save</button>
                <button class="btn btn-warning" type="submit">Save & Add Again</button>
                <input class="btn btn-danger" type="reset" value="Reset" />
            </form>
        </div>
    </body>
</html>
