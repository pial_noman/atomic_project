<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <title>Atomic Project</title>
        
        <style>
            h1,h3,a{
                text-align: center;
            }
            .save_button {
                min-width: 250px;
                max-width: 250px;
            }
            html body {
                background-color: #4dd2ff;

            }
        </style>
        
    </head>
    <body>
        <div  class="container" align="center">
        
            <h1>Web App Development - PHP</h1>
            <h3>Name: Md. Abdullah Al Noman</h3>
            <h3>ID: 107741</h3>
            <h3>Batch: 10</h3>
        <?php
        // put your code here

        ?>
        
        <div class="save_button">
            <a href="views/SEIP107741/book_title/index.php" class="btn btn-primary btn-lg save_button">Book Title</a><br><br>
            <a href="views/SEIP107741/date/index.php" class="btn btn-primary btn-lg save_button">Birthday</a><br><br>
            <a href="views/SEIP107741/text_area/index.php" class="btn btn-primary btn-lg save_button">Summary of Organization</a><br><br>
            <a href="views/SEIP107741/email/index.php" class="btn btn-primary btn-lg save_button">email subscription</a><br><br>
            <a href="views/SEIP107741/file/index.php" class="btn btn-primary btn-lg save_button">Profile Picture</a><br><br>
            <a href="views/SEIP107741/radio/index.php" class="btn btn-primary btn-lg save_button">Gender</a><br><br>
            <a href="views/SEIP107741/checkbox_single/index.php" class="btn btn-primary btn-lg save_button">Terms and Condition</a><br><br>
            <a href="views/SEIP107741/checkbox_multiple/index.php" class="btn btn-primary btn-lg save_button">Hobby</a><br><br>
            <a href="views/SEIP107741/select/index.php" class="btn btn-primary btn-lg save_button">City</a>
        </div><br>
        
        </div>
    </body>
</html>
